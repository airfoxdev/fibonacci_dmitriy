package ca.consulti.airfoxfibonacci;

import javax.inject.Singleton;

import ca.consulti.airfoxfibonacci.modules.AppModule;
import ca.consulti.airfoxfibonacci.services.FibonacciService;
import ca.consulti.airfoxfibonacci.ui.MainActivity;
import dagger.Component;

/**
 * Dagger interface
 * Uses to inject AppComponent into application classes
 */
@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {
    // declare injection for MainActivity
    void inject(MainActivity activity);

    // declare injection for FibonacciService
    void inject(FibonacciService service);

}
