package ca.consulti.airfoxfibonacci;

import android.app.Application;

import ca.consulti.airfoxfibonacci.modules.AppModule;

/**
 * Application class
 * Uses to override super methods for our goals
 */
public class App extends Application {

    /**
     * AppComponent instance
     */
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        injectModule();
    }

    /**
     * initiate AppComponent instance and inject AppModule
     */
    private void injectModule() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
