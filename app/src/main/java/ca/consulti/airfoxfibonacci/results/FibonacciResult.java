package ca.consulti.airfoxfibonacci.results;

import java.math.BigDecimal;

/**
 * Pojo for post result as FibonacciResult object
 */
public class FibonacciResult {

    private BigDecimal result;

    public FibonacciResult(BigDecimal result) {
        this.result = result;
    }

    public BigDecimal getResult() {
        return result;
    }

    public void setResult(BigDecimal result) {
        this.result = result;
    }
}
