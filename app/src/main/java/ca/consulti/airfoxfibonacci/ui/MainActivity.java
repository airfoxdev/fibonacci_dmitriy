package ca.consulti.airfoxfibonacci.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ca.consulti.airfoxfibonacci.App;
import ca.consulti.airfoxfibonacci.R;
import ca.consulti.airfoxfibonacci.results.FibonacciResult;
import ca.consulti.airfoxfibonacci.services.FibonacciService;

/**
 * Simple Activity
 * Uses to interact with user
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Injection of default event-bus into Activity
     */
    @Inject Bus mBus;

    /*
     * Bind Views
     */
    @BindView(R.id.main_number_input) EditText numberInput;
    @BindView(R.id.main_result_field) TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Register this Activity to get injections
        ((App) getApplication()).getAppComponent().inject(this);

        // Register this Activity to Bind Views
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register this Activity to listen Default Event Bus
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // unregister this Activity from listen Default Event Bus
        mBus.unregister(this);
    }

    /**
     * Bind on calc button click action
     * @param view
     */
    @OnClick(R.id.main_fibonacce_calc_btn)
    public void onCalcClick(View view){

        // hide result view when calc pressed again
        resultTextView.setVisibility(View.GONE);

        // handle error
        if (! hasError(numberInput.getText().toString())) {

            int i = Integer.parseInt(numberInput.getText().toString().trim());

            // start service
            startFibonacciService(i);
        }

    }

    /**
     * Starts Fibonacci service to calc result from "i"
     * @param i
     */
    private void startFibonacciService(int i) {
        Intent intent = new Intent(this, FibonacciService.class);
        intent.putExtra("i", i);
        startService(intent);
    }

    /**
     * Subscribe to handle Event with result in Default event-bus
     * @param result
     */
    @Subscribe
    public void onFibonacciResult(FibonacciResult result){

        // show result on UI
        resultTextView.setText(getString(R.string.result) + result.getResult().toString());
        resultTextView.setVisibility(View.VISIBLE);

        // stop service
        stopService(new Intent(this, FibonacciService.class));
    }

    /**
     * Handle input errors
     * @param s
     * @return boolean isError
     */
    private boolean hasError(String s){
        boolean isError = false;

        if (s.equals("")) {
            isError = true;
            numberInput.setError(getString(R.string.error_empty_value));
        } else if (Integer.parseInt(s) > 1000 || Integer.parseInt(s) == 0) {
            isError = true;
            numberInput.setError(getString(R.string.error_wrong_value));
        }

        return isError;
    }

}
