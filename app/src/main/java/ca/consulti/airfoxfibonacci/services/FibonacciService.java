package ca.consulti.airfoxfibonacci.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

import com.squareup.otto.Bus;

import java.math.BigDecimal;

import javax.inject.Inject;

import ca.consulti.airfoxfibonacci.App;
import ca.consulti.airfoxfibonacci.results.FibonacciResult;

/**
 * Service for calc fibonacci
 */
public class FibonacciService extends Service{

    /**
     *  Handler instance
     */
    private Handler mHandler;

    // Inject Default event bus
    @Inject Bus mBus;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Register this Service to get injections
        ((App) getApplication()).getAppComponent().inject(this);

        // setup Handler instance
        initHandler();

        // check extra for integer, and run background task
        if (intent.hasExtra("i")) {
            int i = intent.getIntExtra("i", 0);
            runTask(i);
        }

        return START_NOT_STICKY;
    }

    /**
     * Initiate and setup Handler instance
     */
    private void initHandler() {
        mHandler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                // Retrieve result from Message
                BigDecimal result = (BigDecimal) msg.getData().getSerializable("result");

                // Post result event into Default event bus
                mBus.post(new FibonacciResult(result));
            }
        };
    }

    /**
     * Starts new Thread for calculate
     * @param i
     */
    private void runTask(final int i) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                // uncomment this to check recursion to calc fibonacci
                BigDecimal result = fibonacci(i);

                // uncoment this to check optimized function to calc fibonacci
//                BigDecimal result = fibonacciOptimized(i);

                Bundle bundle = new Bundle();
                bundle.putSerializable("result", result);

                Message message = new Message();
                message.setData(bundle);

                mHandler.sendMessage(message);
            }
        }).start();
    }

    /**
     * Not needed in this case, as we use Event Bus to post result for Activity
     * Uses to bind service into Activity and get access to it
     * @param intent
     * @return IBinder
     */
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Fibonacci function
     * The recursion of the example above takes too long to execute
     * i find my function "fibonacciOptimized()" more optimized especially when you working with numbers more than 40
     * @param n
     * @return BigDecimal
     */
     private BigDecimal fibonacci(long n) {

        if (n <= 1)
            return new BigDecimal(n);
        else
            return fibonacci(n - 1).add(fibonacci(n - 2));

     }

    /**
     * Fibonacci function optimized
     * @param n
     * @return BigDecimal
     */
    private BigDecimal fibonacciOptimized(int n) {

        long num = n;

        BigDecimal a = BigDecimal.valueOf(1);
        BigDecimal b = BigDecimal.valueOf(0);
        BigDecimal temp;

        while (num - 1 >= 0){
            temp = a;
            a = a.add(b);
            b = temp;
            num--;
        }

        return b;

    }

}
